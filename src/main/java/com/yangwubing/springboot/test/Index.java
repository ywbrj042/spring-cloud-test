package com.yangwubing.springboot.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by yangwubing on 17/2/28.
 */
@RestController
public class Index {

    @RequestMapping("/")
    public String index(){
        return "hello,world, this is from server.";
    }

}
